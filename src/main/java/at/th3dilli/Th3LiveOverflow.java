package at.th3dilli;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.text.Text;
import net.minecraft.util.math.Vec3d;
import org.lwjgl.glfw.GLFW;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Th3LiveOverflow implements ClientModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger("th3bypass");
    public static double FALL_VELOCITY = -0.04;
    private int flyTicks;
    private boolean flyEnabled;

    @Override
    public void onInitializeClient() {

        KeyBinding flyKey = KeyBindingHelper.registerKeyBinding(new KeyBinding("flying", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_R, "th3liveoverflow"));
        KeyBinding teleport = KeyBindingHelper.registerKeyBinding(new KeyBinding("teleport", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_U, "th3liveoverflow"));

        ClientTickEvents.START_CLIENT_TICK.register(client -> {
            if (flyEnabled) {
                flyTicks--;
//                Th3LiveOverflow.LOGGER.info("flyTicks: " + flyTicks);
                if (flyTicks < 0) {
//                    Th3LiveOverflow.LOGGER.info("resetting fly");
                    if (client.player != null && !client.player.isOnGround() && !client.player.isTouchingWater() && !client.player.isFallFlying()) {
                        Vec3d velocity = client.player.getVelocity();
                        client.player.setVelocity(new Vec3d(velocity.x, FALL_VELOCITY, velocity.z));
                    }
                    flyTicks = 8;
                }
                if (flyTicks == 7) {
//                    Th3LiveOverflow.LOGGER.info("resetting fly up");
                    if (client.player != null && !client.player.isOnGround() && !client.player.isTouchingWater() && !client.player.isFallFlying()) {
                        Vec3d velocity = client.player.getVelocity();
                        client.player.setVelocity(new Vec3d(velocity.x, -FALL_VELOCITY, velocity.z));
                    }
                }
            }
        });

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (flyKey.wasPressed()) {
                ClientPlayerEntity player = client.player;
                if (player != null) {
                    player.getAbilities().flying = flyEnabled = !player.getAbilities().flying;
                    if (flyEnabled) {
                        Vec3d pos = client.player.getPos();
                        client.player.setPos(pos.x, pos.y + 1.0d, pos.z);
                    }
                    player.sendMessage(Text.of("Flying " + (player.getAbilities().flying ? "enabled" : "disabled")), false);
                }
            }
            if (teleport.wasPressed()) {
                if (client.player != null) {
                    Vec3d pos = client.player.getPos();
                    client.player.setPos(pos.x, pos.y + 5, pos.z);
                }
            }
        });
    }
}
